import emotion from "emotion/dist/emotion.umd.min.js";
export const { css, cx } = m;

export let masked = css`
  color: orange;
  opacity: 0.3;
`;
export const label = function(toggle) {
  return css`
    color: ${toggle ? "orange" : "green"};
    ${!toggle ? masked : css``}
  `;
};

export const h1 = (a) => {
  // console.log(cx(css`color:red`,css`text-align:center`))
  return css`
    font-family: system-ui;
    font-size: xx-large;
    opacity:0.139;    
    font-weight: 900;
    color: red;
    ${a
      ? css`
          color: red;
          opacity: 1;
        `
      : css`
          color: green;
        `}
  `;
};
export const textfield = function(a) {
  return css`
  
    display: flex;
    flex-direction: row;
    justify-content:space-between;
    border: 1px solid rgba(0, 0, 0, 0.2);
    border-radius: 4px;
    background: white;
  `;
};
