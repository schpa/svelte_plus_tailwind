import emotion from 'emotion/dist/emotion.umd.min.js';

const { css } = emotion;

const brand = "#99009999"; //'#74D900';

export const title = css`
  color: ${brand};
  font-size: 1em;
  white-space: nowrap;
  text-shadow: 2px 1px 2px rgba(0,0,0,0.94); 
  background-clip: text;  
`;

export const comicSans = css`
  font-family: "Comic Sans MS";
`;

export const box2 = css`
  border: 1px solid red;
  padding: 1rem;
  margin: 1rem;
  border-radius: 1rem;
  
`

export const box = css`
  position: relative;
  display: inline-block;
  border: 2px solid ${brand};
  line-height: 1;
  padding: 4px;
  border-radius: 4px;
  box-shadow: 0 8px 6px -6px black;
  // box-shadow: 0px 0px 8px 4px rgba(0,0,0,0.5);
`;

export const link = css`
  color: inherit;
  font-weight: bold;
  text-decoration: none;
  border-bottom: 1px solid ${brand};
  &:hover {
    text-decoration: none;
    background: ${brand};
  }
`;
