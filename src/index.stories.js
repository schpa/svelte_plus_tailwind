import { storiesOf } from "@storybook/svelte";
import { action } from "@storybook/addon-actions";

import Button from "./Button.svelte";
import Cell, {cellFunc} from "./Cell.svelte";
import Label from "./Label.svelte";
import Boxed from "./Boxed.svelte";
import Spacer from "./Spacer.svelte";

storiesOf("othersb", module)
  .add("ab", () => ({
    Component: Label
  }));

storiesOf("others", module)
  .add("a", () => ({
    Component: Label
  }));

storiesOf("Button", module)
  .add("with texsst", () => ({
    Component: Button,
    props: { text: "Hello Button" },
    on: { click: action("clicked") }
  }))
  .add("fluff", () => ({
    Component: Cell,
    on: { hover: action("hhover"),
      click: action("deleted")}
  }))
  .add("stuff", () => ({
    Component: Cell,
    props: {
      text: "A",
      toggle: true
    }   
  }))
  .add("spacer", () => ({
    Component: Spacer,
    props: {
      text: "😀 😎 👍 💯"
    },
    on: { click: action("clicked") }
  }))
  .add("boxeds", () => ({
    Component: Boxed,
    props: {
      data: [1, "arguments"]
    },
    on: {}
  }));
